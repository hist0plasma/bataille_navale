package game_interface;

//Import event buttons
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//Import fichiers ttf
import java.io.File;
//Import try/catch exception
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
//Import for sounds
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

//Import interface
import javax.swing.*;

/** Class is to show the menu
 * @author Marion PERRIER
 * @date of the last edition : 29/04/2019
 */

public class Menu {
	//Ressource
    Font bonesTitle;
    Font bonesButton;
    
    //Interface
    JFrame frame;
    JLabel background;
    JPanel buttonPanel;
    JLabel left;
    JLabel right;
    JLabel gameTitle;
    JLabel space1;
    JLabel space2;
    JLabel space3;
    JLabel space4;
    JLabel space5;
    JLabel space6;
    JButton player1;
    JButton player2;
    JButton credits;
    JButton quit;
    
	private void createAndShowGUI() {
		//main window
		frame = new JFrame("MENU");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Background loading
        frame.setLayout(new BorderLayout());
        background=new JLabel(new ImageIcon("img/pirate_background.png"));
        frame.add(background);
        background.setLayout(new GridLayout(1,3));
        buttonPanel = new JPanel(new GridLayout(0,1));
        
        //Font loading and use
        try {
        	bonesTitle = Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")).deriveFont(60f);
        	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        	ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")));
        }
        catch(IOException | FontFormatException e) {
        	e.getStackTrace();
        }
        
        try {
        	bonesButton = Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")).deriveFont(45f);
        	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        	ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")));
        }
        catch(IOException | FontFormatException e) {
        	e.getStackTrace();
        }
        
        left = new JLabel("");
        right = new JLabel("");

        gameTitle=new JLabel("BATTLE SHIP", JLabel.CENTER);
        gameTitle.setFont(bonesTitle);
        gameTitle.setForeground(Color.black);
        space1=new JLabel("");
        space2=new JLabel("");
        space3=new JLabel("");
        space4=new JLabel("");
        space5=new JLabel("");
        space6=new JLabel("");
        
        //Button creation 
        player1=new JButton("one player");
        player1.setFont(bonesButton);
        player1.setSize(20, 20);
        player2=new JButton("two players");
        player2.setFont(bonesButton);
        credits=new JButton("credits");
        credits.setFont(bonesButton);
        quit=new JButton("exit");
        quit.setFont(bonesButton);
        
        //Make a nice interface with nice spacing
        buttonPanel.add(space6);
        buttonPanel.add(gameTitle);
        buttonPanel.add(space1);
        buttonPanel.add(player1);
        buttonPanel.add(space2);
        buttonPanel.add(player2);
        buttonPanel.add(space3);
        buttonPanel.add(credits);
        buttonPanel.add(space4);
        buttonPanel.add(quit);
        buttonPanel.add(space5);
        
        buttonPanel.setOpaque(false);
        
        //Adding to the main windows
        background.add(left);
        background.add(buttonPanel);
        background.add(right);
        
        //buttons action
  		quit.addActionListener(new ActionListener() {
  			public void actionPerformed(ActionEvent e) {
  				frame.dispose();
  			}
  		});
        
  		player1.addActionListener(new ActionListener() {
  			//Ask for name, and then set up the boat placement
  			public void actionPerformed(ActionEvent e) {
  			    String name = JOptionPane.showInputDialog(null, "What's yar cap'n name ?", JOptionPane.QUESTION_MESSAGE);
  			    GameState.player1.setName(name);
  				frame.dispose();
  			}
  		});
  		
  		player2.addActionListener(new ActionListener() {
  			//Ask for names, and then set up the boat placement for both players
  			public void actionPerformed(ActionEvent e) {
  				String name1 = JOptionPane.showInputDialog(null, "CAP'N 1 ! What's yarr pirate's name ?", null, JOptionPane.QUESTION_MESSAGE);
  				String name2 = JOptionPane.showInputDialog(null, "CAP'N 2 ! What's yarr pirate's name ?", null, JOptionPane.QUESTION_MESSAGE);
  				GameState.player1.setName(name1);
  				GameState.player2.setName(name2);
  				frame.dispose();
  				new BoatPlacement(GameState.player1);
  			}
  		});
  		
  		credits.addActionListener(new ActionListener() {
  			//Pop up for credits
  			public void actionPerformed(ActionEvent e) {
  				JOptionPane.showMessageDialog(null, "Background image : reddit/u/GamingHand\nShip pack by Kenney\nMusic Audio by Otto Halmn\nSounds by : \nVideo game made by : Marion PERRIER");
  			}
  		});
  		
		//set main windows full screen and visible
	    frame.setUndecorated(false);
	    frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	    frame.setVisible(true);
	    audioSound();
	}
	
	//sounds part - "clip.open" doesn't work : to fix
	public void audioSound() {
        File sound = new File("sounds/thunderchild.wav");
        try {
            Clip clip = AudioSystem.getClip();
            AudioInputStream inputStream = AudioSystem.getAudioInputStream(sound);
            clip.open(inputStream);
            clip.start();
            Thread.sleep(clip.getMicrosecondLength()/1000); // /1000 because of the microseconds
        }catch(Exception e){
            e.getStackTrace();
        }    
    }
	
	public static void main (String[] args) {
		new Menu().createAndShowGUI();
	}
}
