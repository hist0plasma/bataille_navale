package game_interface;

public class Point {
	//Variable declaration
	int i;
	int j;
	boolean isHit;
	
	//default constructor
	Point(){
		i = -1;
		j = -1; //As int can't be null, -1 represent a null case here
		isHit = false;
	}
	
	//constructor with atributes
	Point(int i, int j){
		this.i = i;
		this.j = j;
		isHit = false;
	}
	
	/** This function is to save the placement of the boat
	 * @author Marion PERRIER
	 * @param x is the position of the first case of the boat
	 * @param y is the position of the last case of the boat
	 */
	public void setPosition(Point x) {
		this.i = x.i;
		this.j = x.j;
	}
}
