package game_interface;

//Import event buttons
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//Graphics imports
import javax.swing.*;

/** Class is to show the grid of the battle
 * @author Marion PERRIER
 * @date of the last edition : 27/04/2019
 */
public class GameInterface{
	
	//Interface
	JFrame frame;
	JPanel titleNamePlayer;
	JLabel namePlayer;
	JPanel gridLeft;
	JPanel gridRight;
	JButton[][] gridLeftButtons;
	JButton[][] gridRightButtons;
	JPanel buttons;
	JButton quit;
	JButton done;
	
	//Variables
	Player p;
    Boolean isClicked;
    Player opponent;

	GameInterface(Player p) {
		this.p = p;
		if(GameState.isPlayer1) {
			opponent = GameState.player2;
		}
		else {
			opponent = GameState.player1;
		}
		//main window
		frame = new JFrame("BATTLE SHIP");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		//panel top - player name
		titleNamePlayer = new JPanel();
		namePlayer = new JLabel("YOUR TURN " + p.name);
		titleNamePlayer.add(namePlayer);
		
		//middle panel
		gridLeft = new JPanel();
		gridLeft.setLayout(new GridLayout(11,11));
		
		gridRight = new JPanel();
		gridRight.setLayout(new GridLayout(11,11));

		int n_case = 11;
		//left grid filling = opponent screen
		//i is for lines and j for columns
		gridLeftButtons = new JButton[n_case][n_case];
		for (int i = 0; i<n_case; i++) {
			for (int j = 0; j<n_case; j++) {
				//first if/else is to not write the "0" on the case's label
				if (i == 0 && j == 0) {
					JLabel label = new JLabel("");
					gridLeft.add(label);
				}
				else {
					if (i == 0) {
						JLabel label = new JLabel(String.valueOf(j));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						gridLeft.add(label);
					}
					else if (j == 0) {
						JLabel label = new JLabel(String.valueOf(i));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						gridLeft.add(label);
					}
					else {
						gridLeftButtons[i][j] = new JButton();
						gridLeft.add(gridLeftButtons[i][j]);
						gridLeftButtons[i][j].addActionListener(new MyActionListener(i, j, gridLeftButtons) {
							@Override
							public void actionPerformed(ActionEvent e) {
								fire(i, j);
							}
						});
					}
				}
			}
		}
		//Set the color if the button was already clicked by the player
		for(Point point : p.saveWaterPoints) {
			gridLeftButtons[point.i][point.j].setEnabled(false);
			gridLeftButtons[point.i][point.j].setBackground(Color.blue);
		}
		for(Boat boat : opponent.boats) {
			for (Point point : boat.listPoint) {
				if(point.isHit) {
					gridLeftButtons[point.i][point.j].setEnabled(false);
					gridLeftButtons[point.i][point.j].setBackground(Color.red);
				}
				if(!boat.isAlive) {
					gridLeftButtons[point.i][point.j].setBackground(Color.black);
				}
			}
		}
		//grid right filling = set your boats visible
		gridRightButtons = new JButton[n_case][n_case];
		for (int k = 0; k<n_case; k++) {
			for (int l = 0; l<n_case; l++) {
				//first if/else is to not write the "11" on the case's label
				if (k == 0 && l == n_case-1) {
					JLabel label = new JLabel("");
					gridRight.add(label);
				}
				else {
					//Second if/else if/ else is to write others labels
					if (k == 0) {
						JLabel label = new JLabel(String.valueOf(l+1));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						gridRight.add(label);
					}
					else if (l == n_case-1) {
						JLabel label = new JLabel(String.valueOf(k));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						gridRight.add(label);
					}
					else {
						gridRightButtons[k][l] = new JButton();
						gridRightButtons[k][l].setEnabled(false);
						//Checking there is a boat, and change the color of the case if yes
						for (int m = 0; m < p.boats.size() ; m++) {
							for (Point point : p.boats.get(m).listPoint) {
								if(point.i == k && point.j == l+1) {
									if(point.isHit) {
										gridRightButtons[k][l].setBackground(Color.red);
									}
									else {
										gridRightButtons[k][l].setBackground(Color.DARK_GRAY);
									}
								}
							}
						}
						gridRight.add(gridRightButtons[k][l]);
					}
				}
			}
		}
		
		JSplitPane gridSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, gridLeft, gridRight);
		
		//panel bottom - buttons
		buttons = new JPanel(new FlowLayout(FlowLayout.CENTER));
		quit = new JButton("Quit");
		buttons.add(quit);
		
		//buttons action
		quit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
		
		//add to the main windows
		frame.add(titleNamePlayer, BorderLayout.PAGE_START);
		frame.add(gridSplit, BorderLayout.CENTER);
		gridSplit.setResizeWeight(0.5);
		gridSplit.setEnabled(false);
		frame.add(buttons, BorderLayout.PAGE_END);
		
		//set main windows full screen and visible
	    frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	    frame.setUndecorated(false);
	    frame.setVisible(true);
	}
	
	public void fire(int i, int j) {
		boolean isTouched = false;
		
		//The button gets disabled
		gridLeftButtons[i][j].setEnabled(false);

		for(int k = 0 ; k < opponent.boats.size() ; k++) {
			for (Point point : opponent.boats.get(k).listPoint) {
				//If there is a enemy ship, the button will be red
				if(point.i == i && point.j == j) {
					opponent.boats.get(k).setHit(i, j);
					gridLeftButtons[i][j].setBackground(Color.red);
					//If the other player is dead, then the game is closed. (I know, that's brutal. But we're pirates, remember ?)
					if(opponent.isLost()) {
						JOptionPane.showMessageDialog(null, "CONGRATS PIRATE ! You won the treasure !");
		  				frame.dispose();
		  				Menu.main(null);
		  				return;
					}
					if(opponent.boats.get(k).isAlive) {
		  				JOptionPane.showMessageDialog(null, "You touched your opponent !", null, JOptionPane.WARNING_MESSAGE);
		  				frame.dispose();
		  				JOptionPane.showMessageDialog(null, "PIRATE "+opponent.name+" are you ready ?", null, JOptionPane.WARNING_MESSAGE);
		  				isTouched = true;
		  				GameState.isPlayer1 = !GameState.isPlayer1;
		  				new GameInterface(opponent);
					}
					else {
		  				JOptionPane.showMessageDialog(null, "The enemy ship is destroyed ! Congrat's !", null, JOptionPane.WARNING_MESSAGE);
		  				frame.dispose();
		  				JOptionPane.showMessageDialog(null, "PIRATE "+opponent.name+" are you ready ?", null, JOptionPane.WARNING_MESSAGE);
		  				isTouched = true;
		  				GameState.isPlayer1 = !GameState.isPlayer1;
		  				new GameInterface(opponent);

					}
				}
			}
		}
		if(!isTouched) {
			//If there is no boat, it will be blue (like the water)
			gridLeftButtons[i][j].setBackground(Color.blue);
			JOptionPane.showMessageDialog(null, "Nope, nothing there. Not a soul.", null, JOptionPane.WARNING_MESSAGE);
			p.saveWaterPoints.add(new Point(i, j));
			frame.dispose();
			JOptionPane.showMessageDialog(null, "PIRATE "+opponent.name+" are you ready ?", null, JOptionPane.WARNING_MESSAGE);
			GameState.isPlayer1 = !GameState.isPlayer1;
			new GameInterface(opponent);
		}
	}
    
	//main class to run the interface
	public static void main (String[] args) {
		
	}
}
