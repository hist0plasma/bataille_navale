package game_interface;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public abstract class MyActionListener implements ActionListener{
	int i;
	int j;
	JButton[][] gridButtons;
	
	MyActionListener(int i, int j, JButton[][] gridButtons){
		super();
		this.i = i;
		this.j = j;
		this.gridButtons = gridButtons;
	}
}
