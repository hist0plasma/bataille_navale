package game_interface;

//Graphics imports
import javax.swing.*;
import javax.swing.border.Border;
//Import event buttons
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
//Import try/catch exception
import java.io.File;
//Import try/catch exception
import java.io.IOException;

import java.util.ArrayList;

/** This class is to put your boats on the grid
 * @author Marion PERRIER
 * @date of the last edition : 27/04/2019
 */

public class BoatPlacement {
	//Resource
	Font title;
	
	//Interface
	JFrame frame;
	JLabel background;
	JPanel titleNamePlayer;
	JLabel namePlayer; 
	JPanel gridCenter;
	JPanel grid;
	JButton[][] gridButtons;
	JPanel boatPanel;
	JLabel space1;
	JLabel space2;
	JPanel buttons;
	JButton ready;
	JButton quit;
	ArrayList<JLabel> listImageBoat;
	Border redBorder;
	
	//Variable
	int boatPlaced = 0;
	boolean isHorizontal = false;
	Player p;
	
	BoatPlacement(Player p) {
		this.p = p;
		//main window
		frame = new JFrame("BATTLE SHIP");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		
		//panel top - player name
		titleNamePlayer = new JPanel();

		try {
        	title = Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")).deriveFont(60f);
        	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        	ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("font/SketchBones.ttf")));
        }
        catch(IOException | FontFormatException e) {
        	e.getStackTrace();
        }
		namePlayer = new JLabel("Boat placement, " + p.name + " !");
		//namePlayer.setFont(title);
		titleNamePlayer.add(namePlayer);
		
		//panel middle : grid and boats
		gridCenter = new JPanel(new GridLayout(1,2));
		grid = new JPanel();
		grid.setLayout(new GridLayout(12,12));

		int n_case = 11;
		
		gridButtons = new JButton[n_case][n_case];
		for (int i = 0; i<n_case; i++) {
			for (int j = 0; j<n_case; j++) {
				//first if/else is to not write the "0" on the case's label
				if (i == 0 && j == 0) {
					JLabel label = new JLabel("");
					grid.add(label);
				}
				else {
					if (i == 0) {
						JLabel label = new JLabel(String.valueOf(j));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						grid.add(label);
					}
					else if (j == 0) {
						JLabel label = new JLabel(String.valueOf(i));
						label.setHorizontalAlignment(JLabel.CENTER);
						label.setVerticalAlignment(JLabel.CENTER);
						grid.add(label);
					}
					else {
						gridButtons[i][j] = new JButton();
						grid.add(gridButtons[i][j]);	
						//gridButtons[i][j].setActionCommand("Position "+ String.valueOf(i) + " " +String.valueOf(j));
						gridButtons[i][j].addMouseListener(new MyMouseAdapter(i, j, gridButtons) {
							@Override
							public void mouseExited(MouseEvent e) {
							//Disable the shadow of the boat when the mouse is leaving the button
								if(boatPlaced >= (p.boats).size()) {
									return;
								}
								int sizeBoat = Boat.getSize(p.boats.get(boatPlaced).getType());
								if(isHorizontal) {
									if(j < sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(null);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i][j-k].setBackground(null);
									}
								}
								else {
									if(i > n_case-sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(null);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i+k][j].setBackground(null);
									}
								}
							}
							@Override
							public void mouseEntered(MouseEvent e) {
							//Get a shadow on where the boat is set
								if(boatPlaced >= (p.boats).size()) {
									return;
								}
								int sizeBoat = Boat.getSize(p.boats.get(boatPlaced).getType());
								if(isHorizontal) {
									if(j < sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(Color.darkGray);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i][j-k].setBackground(Color.darkGray);
									}
								}
								else {
									if(i > n_case-sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(Color.darkGray);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i+k][j].setBackground(Color.darkGray);
									}
								}
							}
							@Override
							public void mouseClicked(MouseEvent e) {
							//A right click is setting isHorizontal at false or true
								if(e.getButton() != MouseEvent.BUTTON3) {
									return;
								}
								int sizeBoat = Boat.getSize(p.boats.get(boatPlaced).getType());
								//takes off the button
								if(isHorizontal) {
									if(j < sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(null);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i][j-k].setBackground(null);
									}
								}
								else {
									if(i > n_case-sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(null);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i+k][j].setBackground(null);
									}
								}
								//invert boolan
								isHorizontal = !isHorizontal;
								
								//recolor the button
								if(isHorizontal) {
									if(j < sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(Color.darkGray);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i][j-k].setBackground(Color.darkGray);
									}
								}
								else {
									if(i > n_case-sizeBoat) {
										return;
									}
									gridButtons[i][j].setBackground(Color.darkGray);
									for(int k = 1; k < sizeBoat; k++) {
										gridButtons[i+k][j].setBackground(Color.darkGray);
									}
								}
							}
						});
						gridButtons[i][j].addActionListener(new MyActionListener(i, j, gridButtons) {
							@Override
							public void actionPerformed(ActionEvent e) {
								placeBoat(i, j, n_case);
							}
						});
					}
				}
			}
		}
		
		gridCenter.add(grid);
		
		//Boat panel
		boatPanel = new JPanel(new GridLayout(7,1));
		
		//Get boat image
		space1 = new JLabel("");
		space2 = new JLabel("");
		boatPanel.add(space1);
		listImageBoat = new ArrayList<>();
		for(int i = 0; i<p.boats.size(); i++) {
			listImageBoat.add(new JLabel(new ImageIcon(Boat.getImage(p.boats.get(i).getType()))));
			//add it
			boatPanel.add(listImageBoat.get(i));
		}
		boatPanel.add(space2);
		
		//Color in red the border when selected
		redBorder = BorderFactory.createLineBorder(Color.red);
		listImageBoat.get(boatPlaced).setBorder(redBorder);

		gridCenter.add(boatPanel);
		
		//Button finish panel
		buttons = new JPanel();
		ready = new JButton("Ready !");
		ready.setEnabled(false);
		
		quit = new JButton("Exit");
		buttons.add(ready, BorderLayout.CENTER);
		buttons.add(quit, BorderLayout.CENTER);
		
		//Button actions
		quit.addActionListener(new ActionListener() {
  			public void actionPerformed(ActionEvent e) {
  				frame.dispose();
  			}
  		});
		
		//Enable button to launch player2 boat placement and launch game interface
		ready.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(GameState.isPlayer1) {
					new BoatPlacement(GameState.player2);
					GameState.isPlayer1 = !GameState.isPlayer1;
					frame.dispose();
				}
				else {
					GameState.isPlayer1 = !GameState.isPlayer1;
					new GameInterface(GameState.player1);
					frame.dispose();
				}
			}
		});
		
		//add to the main windows
		
		frame.add(titleNamePlayer, BorderLayout.PAGE_START);
		frame.add(gridCenter, BorderLayout.CENTER);
		frame.add(buttons, BorderLayout.PAGE_END);
		
		//set main windows full screen and visible
	    frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	    frame.setUndecorated(false);
	    frame.setVisible(true);

	}
	
	public void placeBoat(int i, int j, int n_case) {
		//we place the boat !
		if(boatPlaced >= (p.boats).size()) {
			return;
		}

		int sizeBoat = Boat.getSize(p.boats.get(boatPlaced).getType());
		if(isHorizontal) {
			//in case of an invalid placement
			if(j < sizeBoat) {
				return;
			}
			//We check overlap on all the size of the boat
			for(int k = 0; k < sizeBoat ; k ++) {
				if(overlapBoat(i, j-k)) {
					return;
				}
			}
			//No overlap. We place the boat.
			for(int k = 0; k < sizeBoat ; k ++) {
				p.boats.get(boatPlaced).listPoint.get(k).setPosition(new Point(i,j-k));
				gridButtons[i][j-k].setEnabled(false);
			}
		}
		else {
			//in case of an invalid placement
			if(i > n_case-sizeBoat) {
				return;
			}
			//We check overlap on all the size of the boat
			for(int k = 0; k < sizeBoat ; k ++) {
				if(overlapBoat(i+k, j)) {
					return;
				}
			}
			//No overlap. We place the boat.
			for(int k = 0; k < sizeBoat ; k ++) {
				p.boats.get(boatPlaced).listPoint.get(k).setPosition(new Point(i+k,j));
				gridButtons[i+k][j].setEnabled(false);
			}
		}
		//Updating border
		listImageBoat.get(boatPlaced).setBorder(null);

		boatPlaced ++;
		if(boatPlaced < p.boats.size()) {
			listImageBoat.get(boatPlaced).setBorder(redBorder);
		}
		
		//Removing background
		if(isHorizontal) {
			for(int k = 0; k < sizeBoat ; k ++) {
				gridButtons[i][j-k].setBackground(null);
			}
		}
		else {
			for(int k = 0; k < sizeBoat ; k ++) {
				gridButtons[i+k][j].setBackground(null);
			}
		}
		//Boat placement finished
		if (boatPlaced >= p.boats.size()) {
			ready.setEnabled(true);
		}
	}
	
	public boolean overlapBoat(int i, int j) {
		for (int k = 0; k < p.boats.size() ; k++) {
			if(p.boats.get(k).listPoint.size() == 0) {
				return false;
			}
			for (Point point : p.boats.get(k).listPoint) {
				if(point.i == i && point.j == j) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static void main (String[] args) {
		
	}
}
