package game_interface;

public class Position {
	//Variable declaration
	Point x;
	Point y;
	
	//default constructor
	Position(){
		x = new Point();
		y = new Point();
	}
	
	//constructor with arguments x and y
	Position(Point x, Point y){
		this.x = x;
		this.y = y;
	}
	
	//constructor with letters and numbers
	Position(int xi, int yi, int xj, int yj){
		x = new Point(xi, xj);
		y = new Point(yi, yj);
	}
}
