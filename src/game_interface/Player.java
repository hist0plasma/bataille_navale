package game_interface;

import java.util.ArrayList;

/** This class save informations about the player
 * @author Marion PERRIER
 * @param num = the default number given to a player
 * @param y is the position of the last case of the boat
 */

public class Player {
	//Variable initialization
	String name;
	static int num = 1; //For player1 or player2
	int numberShots;
	int numberHits;
	ArrayList<Boat> boats = new ArrayList<Boat>();
	ArrayList<Point> saveWaterPoints = new ArrayList<Point>();
	
	//Basic constructor
	Player(){
		this("Player "+String.valueOf(num++));
	}
	
	//Constructor with a name
	Player(String name){
		this.name = name;
		numberShots = 0;
		numberHits = 0;
		//list of boats initialization
		boats.add(new Boat(Boat.EBoatName.DESTROYER)); 
		boats.add(new Boat(Boat.EBoatName.SUBMARINE));
		boats.add(new Boat(Boat.EBoatName.CUISER));
		boats.add(new Boat(Boat.EBoatName.BATTLESHIP));
		boats.add(new Boat(Boat.EBoatName.CARRIER));
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	//Is there is at least one ship alive ?
	public boolean isLost() {
		for (int i = 0 ; i < this.boats.size() ; i++) {
			if (boats.get(i).isAlive) {
				return false;
			}
			
		}
		return true;
	}
	public void setWaterPoint(int i, int j) {
		this.saveWaterPoints.add(new Point(i, j));
	}
}
