package game_interface;

import java.util.ArrayList;

public class Boat {
	public enum EBoatName {
		CARRIER,
		BATTLESHIP,
		CUISER,
		SUBMARINE,
		DESTROYER;
	}
	
	//Variables declaration
	EBoatName typeBoat;
	String name;
	Boolean isAlive;
	int lengthBoat;
	ArrayList<Point> listPoint = new ArrayList<Point>();
	
	//Classic constructor initialization
	Boat(EBoatName typeBoat){
		isAlive = true; //true is Normal, false is Destroyed
		this.typeBoat = typeBoat;
		name = getName(typeBoat);
		lengthBoat = getSize(typeBoat);
		
		//list of positions initialization
		for (int i = 0; i< lengthBoat; i++) {
			listPoint.add(new Point());
		}
	}
	
	/** This function is to get the name of the boat
	 * @author Marion PERRIER
	 * @param typeBoat is to get the right name in String form
	 */
	public static String getName(EBoatName typeBoat) {
		switch(typeBoat) {
		case CARRIER :
			return "Carrier";
		case BATTLESHIP :
			return "Battleship";
		case CUISER :
			return "Cuiser";
		case SUBMARINE :
			return "Submarine";
		case DESTROYER :
			return "Destroyer";
		//Set a default here
		default:
			System.out.println("Unknown type boat in getName");
			return "Unknow";
		}
	}
	
	/** This function is to get the size of the boat
	 * @author Marion PERRIER
	 * @param typeBoat is the name of the boat given by the enum
	 */
	public static int getSize(EBoatName typeBoat) {
		switch(typeBoat) {
		case CARRIER :
			return 5;
		case BATTLESHIP :
			return 4;
		case CUISER :
			return 3;
		case SUBMARINE :
			return 3;
		case DESTROYER :
			return 2;
		//Set a default here
		default:
			System.out.println("Unknown type boat in getSize");
			return -1;
		}
	}
	
	/** This function is to get the image of the boat
	 * @author Marion PERRIER
	 * @param typeBoat is the name of the boat given by the enum
	 */
	public static String getImage(EBoatName typeBoat) {
		switch(typeBoat) {
		case CARRIER :
			return "img/size5/horizontal/hullLarge_intact.png";
		case BATTLESHIP :
			return "img/size4/horizontal/hullSmall_intact.png";
		case CUISER :
			return "img/size3_2/horizontal/ship_intact_2.png";
		case SUBMARINE :
			return "img/size3/horizontal/intact/ship_intact.png";
		case DESTROYER :
			return "img/size2/horizontal/intact/boat2_intact.png";
		//Set a default here
		default:
			System.out.println("Unknown type boat in getSize");
			return "";
		}
	}
	
	public EBoatName getType() {
		return typeBoat;
	}
	
	public void setHit (int i, int j) {
		for (Point point : this.listPoint) {
			if(point.i == i && point.j == j) {
				point.isHit = true;
				break;
			}
		}
		for (Point point : this.listPoint) {
			if (!point.isHit) {
				return;
			}
		}
		this.isAlive = false;
	}
	
	public static void main(String[] args) {
		
	}
}
